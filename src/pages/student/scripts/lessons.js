import {lessons, timeSlots} from "./constants";

const getElById = (el) => document.getElementById(el);

const lessonForm = getElById('formLessons');
const slotsKeys = Object.keys(timeSlots) ;
const tomorrowSlotKeys = slotsKeys.slice(3);

const createObj = () => {
  const obj = {};

  const typeRadioBtn = lessonForm.querySelector('input[name="type"]:checked');
  const timeRadioBtn = lessonForm.querySelector('input[name="time"]:checked');

  const time = timeSlots[timeRadioBtn.id];
  const type = lessons[typeRadioBtn.id];

  const tomorrow = tomorrowSlotKeys.join('').includes(time);

  obj.name = JSON.parse(localStorage.getItem('login')).name;
  obj.time = time;
  obj.tomorrow = tomorrow;
  obj.title = type.title;
  obj.duration = type.duration;

  return obj;
};

const submitForm = (e) => {
  e.preventDefault();
  const obj = createObj();
  const bookedLes = JSON.parse(localStorage.getItem('lessons')) || [];
  localStorage.setItem('lessons', JSON.stringify([obj, ...bookedLes]));
};

lessonForm.addEventListener('submit', submitForm);
