console.log('planed lessons');

const planedLessons = document.getElementById('planedLessons');

const savedLessons = JSON.parse(localStorage.getItem('lessons')) || [];

const getLesson = (lesson) => {
  const { tomorrow, time, name, title, duration } = lesson;
  const day = tomorrow ? 'Завтра' : 'Сегодня';
  const hours = Math.floor(time + duration/60);
  const min = duration % 60;
  const lessonTime = `${time}.00 - ${(hours + min/100).toFixed(2)}`;

  return `
        <div class="card-box">
            <div class="card-illustration">
                <img src="./images/user_02.png" alt="girl">
            </div>
            <div class="info">
                <p class="sub-title">${day}, ${lessonTime}</p>
                <p class="info-title">${name}</p>
                <p class="info-desc">${title}</p>
            </div>
        </div>`;
};

const generateHTML = () => {
  const lessonsHTML = savedLessons.map((lesson) => {
    return getLesson(lesson);
  }).join('');

  planedLessons.insertAdjacentHTML( 'afterend', lessonsHTML);
};

generateHTML();

